package com.example.talkutalku

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.core.view.get
import androidx.drawerlayout.widget.DrawerLayout
import androidx.navigation.findNavController
import androidx.navigation.fragment.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.navigateUp
import androidx.navigation.ui.setupActionBarWithNavController
import androidx.navigation.ui.setupWithNavController
import androidx.recyclerview.widget.RecyclerView
import com.example.talkutalku.ui.home.ExapleItem
import com.example.talkutalku.ui.home.HomeFragment
import com.google.android.material.navigation.NavigationView
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.example_item.*
import kotlinx.android.synthetic.main.example_item.view.*


class MainActivity : AppCompatActivity() {

    private lateinit var appBarConfiguration: AppBarConfiguration
    private var login: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val toolbar: Toolbar = findViewById(R.id.toolbar)
        setSupportActionBar(toolbar)

        val drawerLayout: DrawerLayout = findViewById(R.id.drawer_layout)
        val navView: NavigationView = findViewById(R.id.nav_view) // active_main
        val navController = findNavController(R.id.nav_host_fragment)  // content_main

        appBarConfiguration = AppBarConfiguration(setOf(
            R.id.nav_home,R.id.settings), drawerLayout)
        setupActionBarWithNavController(navController, appBarConfiguration)
        navView.setupWithNavController(navController)


        login = loadData()
        saveData(login)
        intent = Intent(applicationContext, Settings::class.java)


        if(login.equals("") || login.equals(null)){
            startActivity(intent)
        }

        val sharedPreferences = getSharedPreferences("shared preferences",MODE_PRIVATE)
        val editor = sharedPreferences.edit()
        editor.putString("LK", login)
        editor.apply()


    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.main, menu)
        return true
    }



    override fun onSupportNavigateUp(): Boolean {
        val navController = findNavController(R.id.nav_host_fragment)
        return navController.navigateUp(appBarConfiguration) || super.onSupportNavigateUp()
    }


    fun saveData(loginUser: String?) {
        val sharedPreferences =
            getSharedPreferences("TEST", Context.MODE_PRIVATE)
        val editor = sharedPreferences.edit()
        editor.putString("LOGIN_KEY", loginUser)
        editor.apply()
    }



     fun loadData(): String{
        val sharedPreferences =
            getSharedPreferences("TEST", Context.MODE_PRIVATE)
        val login = sharedPreferences.getString("LOGIN_KEY", "")

         return login.toString()
    }




}

package com.example.talkutalku

import com.google.gson.annotations.SerializedName

class MessageChat {

    @SerializedName("messages")
    var messages: ArrayList<Message> = ArrayList<Message>()

    @SerializedName("message/{id}")
    var edit: ArrayList<Message> = ArrayList<Message>()

}
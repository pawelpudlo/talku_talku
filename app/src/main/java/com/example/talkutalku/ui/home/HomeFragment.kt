package com.example.talkutalku.ui.home

import android.content.Context
import android.net.ConnectivityManager
import android.os.Bundle
import android.os.Handler
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.Toast
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.example.talkutalku.*
import kotlinx.android.synthetic.main.fragment_add.*
import kotlinx.android.synthetic.main.fragment_edit.*
import kotlinx.android.synthetic.main.fragment_home.*
import kotlinx.android.synthetic.main.fragment_home.view.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory


class HomeFragment : Fragment() {

    private lateinit var homeViewModel: HomeViewModel

    var active = true
    val listForGetMessage = ArrayList<ExapleItem>()
    val emptyList = ArrayList<ExapleItem>()



    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?

    ): View? {

        homeViewModel =
            ViewModelProviders.of(this).get(HomeViewModel::class.java)
        val root = inflater.inflate(R.layout.fragment_home, container, false)
        (activity as MainActivity?)?.loadData()




        fun gets(){
            val retrofit = Retrofit.Builder()
                .baseUrl("http://tgryl.pl/shoutbox/")
                .addConverterFactory(GsonConverterFactory.create())
                .build()

            val service = retrofit.create(MessageInterface::class.java)

            val call = service.getMessage()

            call!!.enqueue(object : Callback<List<Message?>?> {


                override fun onFailure(call: Call<List<Message?>?>, t: Throwable) {
                }

                override fun onResponse(call: Call<List<Message?>?>, response: Response<List<Message?>?>) {
                    val messageResponse = response.body()
                    val lista: List<Message?>?  = response.body()
                    if(lista != null) {

                        listForGetMessage.clear()

                        lista.forEach{
                            if(it != null){
                                listForGetMessage.add(ExapleItem(it.login.toString(), it.date.toString(), it.content.toString(),it.id.toString()))
                            }
                        }

                    }


                    var recycle_view: RecyclerView = root.findViewById(R.id.chat)

                        recycle_view.layoutManager = LinearLayoutManager(activity)
                        recycle_view.adapter = ExampleAdapter(this@HomeFragment,listForGetMessage)




                    val swipeHandler = object : SwipeToDeleteCallBack(context) {
                        override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {
                            val adapter = recycle_view.adapter as ExampleAdapter

                            adapter.removeAt(viewHolder.adapterPosition)?.let { createDelete(it) }
                        }
                    }
                    val itemTouchHelper = ItemTouchHelper(swipeHandler)
                    itemTouchHelper.attachToRecyclerView(recycle_view)
                }

            })
        }

        if (checkInternetConnection()) {   //standardowe wysyłanie zapytania
            gets()

        } else {

            Toast.makeText(getActivity(),"No Internet access!", Toast.LENGTH_SHORT).show()
        }

        val pullToRefresh: SwipeRefreshLayout = root.findViewById(R.id.pullToRefresh)
        pullToRefresh.setOnRefreshListener {

            refresher()
            gets()
            pullToRefresh.isRefreshing = false
        }


        root.buttonForADD.setOnClickListener {
            val bundle = Bundle()
            //val myLogin: String? = savedInstanceState.putString()

            parentFragment?.findNavController()?.navigate(R.id.action_HomeFragment_to_AddFragment,bundle)
        }

        val handler = Handler()
        handler.postDelayed(object : Runnable {
            override fun run() {
                if (active) {
                    refresher()
                    gets()
                    handler.postDelayed(this, 30000) //30 000 =30 sekund;
                }
            }
        }, 6000)

        return root
    }

    private fun checkInternetConnection(): Boolean {
        val cm =
            requireContext().getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val nInfo = cm.activeNetworkInfo
        return nInfo != null && nInfo.isAvailable && nInfo.isConnected
    }

    private fun refresher() {
        if (checkInternetConnection()) {
           // get()
            Toast.makeText(getActivity(),"Data updated",Toast.LENGTH_SHORT).show()
        } else {
            Toast.makeText(getActivity(),"No Internet access!",Toast.LENGTH_SHORT).show()
        }
    }
/*
    fun get(){
        val retrofit = Retrofit.Builder()
            .baseUrl("http://tgryl.pl/shoutbox/")
            .addConverterFactory(GsonConverterFactory.create())
            .build()

        val service = retrofit.create(MessageInterface::class.java)

        val call = service.getMessage()

        call!!.enqueue(object : Callback<List<Message?>?> {


            override fun onFailure(call: Call<List<Message?>?>, t: Throwable) {
            }

            override fun onResponse(call: Call<List<Message?>?>, response: Response<List<Message?>?>) {
                val messageResponse = response.body()
                val lista: List<Message?>?  = response.body()
                if(lista != null) {

                    lista.forEach{
                        if(it != null){
                            listForGetMessage.add(ExapleItem(it.login.toString(), it.date.toString(), it.content.toString(),it.id.toString()))

                        }
                    }

                    var recycle_view: RecyclerView = root.findViewById(R.id.chat)
                    recycle_view.layoutManager = LinearLayoutManager(activity)
                    recycle_view.adapter = ExampleAdapter(this@HomeFragment,listForGetMessage)

                }
            }

        })

    }
*/
    private fun createDelete(id:String){

        val retrofit = Retrofit.Builder()
            .baseUrl("http://tgryl.pl/shoutbox/")
            .addConverterFactory(GsonConverterFactory.create())
            .build()

        val service = retrofit.create(MessageInterface::class.java)

        val call = service.deleteMessage(id)

        call!!.enqueue(object : Callback<List<Message?>?> {
            override fun onResponse(
                call: Call<List<Message?>?>,
                response: Response<List<Message?>?>
            ) {
                if (!response.isSuccessful) {
                    println("Code: " + response.code())
                    return
                }
                val temp = response.body()
            }

            override fun onFailure(call: Call<List<Message?>?>, t: Throwable) {
                println(t.message)
            }
        })
    }



}

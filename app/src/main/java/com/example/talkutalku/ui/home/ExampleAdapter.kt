package com.example.talkutalku.ui.home

import android.app.PendingIntent.getActivity
import android.content.Context
import android.content.DialogInterface
import android.content.SharedPreferences
import android.graphics.*
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.SimpleAdapter
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.RecyclerView
import com.example.talkutalku.R
import com.example.talkutalku.Sp
import kotlinx.android.synthetic.main.example_item.view.*



class ExampleAdapter(private val parentFragment: Fragment, private val exampleList: ArrayList<ExapleItem>) : RecyclerView.Adapter<ExampleAdapter.ExampleViewHolder>(){


    var Login =""


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ExampleViewHolder {
        val itemView = LayoutInflater.from(parent.context).inflate(
            R.layout.example_item,
            parent,false)

        return ExampleViewHolder(itemView)
    }


    override fun onBindViewHolder(holder: ExampleViewHolder, position: Int) {
        val currentItem = exampleList[position]



        holder.login.text = currentItem.login
        holder.data.text = currentItem.data
        holder.message.text = currentItem.content

        holder.itemView.setOnClickListener {

            val item_login: String = exampleList[position].login

                val item_date: String = exampleList[position].data
                val item_comment: String = exampleList[position].content
                val item_id: String = exampleList[position].id

                val bundle = Bundle()
                bundle.putString("loginMessage", item_login)
                bundle.putString("date", item_date)
                bundle.putString("comment", item_comment)
                bundle.putString("id", item_id)

                parentFragment.findNavController().navigate(R.id.action_nav_home_to_EditFragment, bundle)

        }




    }



    override fun getItemCount() = exampleList.size

    class ExampleViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val login: TextView = itemView.login
        val data: TextView = itemView.data
        val message: TextView = itemView.content
    }

    fun removeAt(position: Int): String? {
        val id = exampleList[position].id
        exampleList.removeAt(position)
        notifyItemRemoved(position)

        return id;
    }






}





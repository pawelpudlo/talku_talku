package com.example.talkutalku.ui.home


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController
import com.example.talkutalku.MainActivity
import com.example.talkutalku.Message
import com.example.talkutalku.MessageInterface
import com.example.talkutalku.R
import com.example.talkutalku.ui.edit.EditModel
import kotlinx.android.synthetic.main.fragment_edit.view.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory


class EditFragment : Fragment() {

    private lateinit var editModel: EditModel
    private lateinit var jsonPlaceholderAPI: MessageInterface
    private lateinit var message: String

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?

    ): View? {
        editModel =
            ViewModelProviders.of(this).get(EditModel::class.java)
        val root = inflater.inflate(R.layout.fragment_edit, container, false)



        val loginTV: TextView = root.findViewById(R.id.loginEdit);
        val messageET: EditText = root.findViewById(R.id.editMessage);
        val messageTV: TextView = root.findViewById(R.id.editMessage);

        val login: String? = arguments?.getString("loginMessage")
        val content: String? = arguments?.getString("comment")
        val id: String? = arguments?.getString("id")


        loginTV.text = login
        messageTV.text = content

        val url: String = "http://tgryl.pl/shoutbox/"

        val retrofit = Retrofit.Builder()
            .baseUrl(url)
            .addConverterFactory(GsonConverterFactory.create())
            .build()
        jsonPlaceholderAPI = retrofit.create(MessageInterface::class.java)


        root.button.setOnClickListener {

                message = messageET.text.toString()


                    setMessage()


                parentFragment?.findNavController()?.navigate(R.id.action_nav_edit_to_nav_home)

        }

        root.delete.setOnClickListener{

            val ids: String = id.toString()


                deleteMessage(ids)



            parentFragment?.findNavController()?.navigate(R.id.action_nav_edit_to_nav_home)

        }
        return root
    }



    private fun setMessage() {
        val loginMessage: String? = arguments?.getString("loginMessage")
        val idM: String = arguments?.getString("id").toString()
        val message = Message(loginMessage, message)

        val call = jsonPlaceholderAPI.editMessage(idM, message)

        call!!.enqueue(object : Callback<List<Message?>?> {

            override fun onFailure(call: Call<List<Message?>?>, t: Throwable) {
                println(t.message)
            }

            override fun onResponse(
                call: Call<List<Message?>?>,
                response: Response<List<Message?>?>
            ) {
                if (!response.isSuccessful) {
                    println("Code: " + response.code())
                    return
                }
            }
        })
    }

    private fun deleteMessage(id: String){
        val retrofit = Retrofit.Builder()
            .baseUrl("http://tgryl.pl/shoutbox/")
            .addConverterFactory(GsonConverterFactory.create())
            .build()

        val service = retrofit.create(MessageInterface::class.java)

        val call = service.deleteMessage(id)

        call!!.enqueue(object : Callback<List<Message?>?> {
            override fun onResponse(
                call: Call<List<Message?>?>,
                response: Response<List<Message?>?>
            ) {
                if (!response.isSuccessful) {
                    println("Code: " + response.code())
                    return
                }
                val temp = response.body()
            }

            override fun onFailure(call: Call<List<Message?>?>, t: Throwable) {
                println(t.message)
            }
        })
    }



}
package com.example.talkutalku

import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.EditText
import android.widget.TextView

class Settings : AppCompatActivity() {



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_settings)



    }

    fun setUserLogin(view: View){
        val login: EditText = findViewById(R.id.editSetLogin)
        val loginUser: String = login.toString();
        intent = Intent(applicationContext, MainActivity::class.java)
       // intent.putExtra("login_key",loginUser)
        saveData(loginUser)

        startActivity(intent)
    }


    private fun loadData(): String {
        val sharedPreferences =
            getSharedPreferences("shared preferences", Context.MODE_PRIVATE)
        return sharedPreferences.getString("login", "")!!
    }

    fun saveData(loginInput: String?) {
        val sharedPreferences =
            getSharedPreferences("shared preferences", Context.MODE_PRIVATE)
        val editor = sharedPreferences.edit()
        val login = loginInput.toString()
        editor.putString("login", login)
        editor.apply()
        setResult(200)
        finish()
    }


}

package com.example.talkutalku



import retrofit2.Call
import retrofit2.http.*


interface MessageInterface {

    @GET("messages")
    fun getMessage(): Call<List<Message?>?>?

    @PUT("message/{id}")
    fun editMessage(
        @Path("id") id: String,
        @Body mess: Message
    ): Call<List<Message?>?>?

    @POST("message")
    fun sendMessage(@Body mess: Message
    ): Call<List<Message?>?>?

    @DELETE("message/{id}")
    fun deleteMessage(
        @Path("id") id: String
    ): Call<List<Message?>?>?
}
package com.example.talkutalku.ui.home


import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.SimpleAdapter
import android.widget.TextView
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.RecyclerView
import com.example.talkutalku.MainActivity
import com.example.talkutalku.Message
import com.example.talkutalku.MessageInterface
import com.example.talkutalku.R
import kotlinx.android.synthetic.main.fragment_add.view.*
import kotlinx.android.synthetic.main.fragment_edit.view.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*


class AddFragment : Fragment() {


    private lateinit var addModel: AddModel
    private lateinit var jsonPlaceholderAPI: MessageInterface
    private lateinit var message: String


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?

    ): View? {

        addModel =
            ViewModelProviders.of(this).get(AddModel::class.java)
        val root = inflater.inflate(R.layout.fragment_add, container, false)

        val myLogin = arguments?.getString("myLog")
        val loginTV: TextView = root.findViewById(R.id.loginAdd);
        val messageAdd: EditText = root.findViewById(R.id.messageAdd);



        loginTV.setText("PaulBox")

        val url: String = "http://tgryl.pl/shoutbox/"

        val retrofit = Retrofit.Builder()
            .baseUrl(url)
            .addConverterFactory(GsonConverterFactory.create())
            .build()
        jsonPlaceholderAPI = retrofit.create(MessageInterface::class.java)

        root.addMessage.setOnClickListener {
            message = messageAdd.text.toString()

            addMessage()

            parentFragment?.findNavController()?.navigate(R.id.action_AddFragment_to_HomeFragment)
        }




        return root
    }

    private fun addMessage() {
/*
        var xd: String = ""

        for (i in 0..5) {
           xd += "⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⠙⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿\n⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⡄⡌⠿⣿⡿⢛⣉⢻⣿⣿⣿⣿⣿⣿⣿\n⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⠁⣷⣶⣆⣴⣿⣿⢸⣿⣿⣿⣿⣿⣿⣿\n⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⢀⣿⣿⣿⣿⣿⣿⣤⡌⢛⡉⢉⣭⣿⣿\n⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣏⣈⣉⡛⠻⣿⣿⣿⣿⠟⢁⣴⣿⣿⣿⣿\n⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⡿⠛⠉⠉⠉⠙⠻⢷⣦⡙⠿⢋⣴⣿⣿⣿⣿⣿⣿\n⣿⣿⣿⣿⣿⠋⢉⣀⣀⠀⠉⠀⣴⡞⠛⠿⢷⣦⣄⡈⠻⣷⣾⣿⣿⣿⣿⣿⣿⣿\n⣿⣿⣿⣿⡇⣴⣿⣿⣿⣿⣿⣿⣿⣷⣤⣴⣶⣿⣿⣿⣷⣦⣝⣿⣿⣿⣿⣿⣿⣿\n⣿⣿⣿⣿⣇⣿⡿⠿⠿⠿⠿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿\n⣿⣿⣿⣿⣿⣿⣿⣿⣿⣷⣶⣄⠉⠻⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿\n⣏⠉⠉⠉⠉⠉⠉⠉⠙⠛⢿⣿⣧⠀⠈⢿⣿⣿⣿⣿⡿⠟⠉⠉⢉⠉⠉⠉⢿⣿\n⣿⡇⠀⠀⢸⣿⣿⣶⡄⠀⠀⢻⣿⡄⠀⠈⣿⣿⣿⡿⠁⢠⣾⣿⣿⣿⣷⣦⣠⣿\n⣿⡇⠀⠀⢸⣿⣿⣿⠇⠀⠀⣼⣿⡟⠛⠛⢻⣿⣿⣷⠀⠀⠙⠛⠿⠿⣿⣿⣿⣿\n⣿⡇⠀⠀⢸⣏⣉⣀⣀⣤⣾⣿⣿⡇⠀⠀⢸⣿⣿⣿⣷⣤⣀⠀⠀⠀⠀⠉⠻⣿\n⣿⡇⠀⠀⢸⣿⣿⣿⣿⣿⣿⣿⣿⡇⠀⠀⢸⣿⣿⠛⢻⣿⣿⣿⣷⣶⣦⡄⠀⢸\n⣿⡇⠀⠀⢸⣿⣿⣿⣿⣿⣿⣿⣿⡇⠀⠀⢸⣿⣿⠀⠘⢿⣿⣿⣿⣿⡿⠏⠀⣸\n⣏⣀⣀⣀⣈⣹⣿⣿⣿⣿⣿⣿⣉⣁⣀⣀⣈⣹⣿⣀⣀⣀⣀⣀⣀⣀⣀⣤⣾⣿\n"
        }
*/
        val myLogin = "PaulBox"
        val message = Message(myLogin, message)

        val call = jsonPlaceholderAPI.sendMessage(message)


        call!!.enqueue(object : Callback<List<Message?>?> {

            override fun onFailure(call: Call<List<Message?>?>, t: Throwable) {
                println("You cant seend message")
            }
            override fun onResponse(call: Call<List<Message?>?>,
                                    response: Response<List<Message?>?>) {
                if (!response.isSuccessful) {
                    println("Code: " + response.code())
                    return
                }
                Toast.makeText(context, "Message be send", Toast.LENGTH_SHORT).show()
            }
        })


    }




}
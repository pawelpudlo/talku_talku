package com.example.talkutalku

import android.content.Context
import androidx.appcompat.app.AppCompatActivity

class Sp : AppCompatActivity()  {

    var login: String? = null

    fun saveData(loginUser: String?) {
        val sharedPreferences =
            getSharedPreferences("TEST", Context.MODE_PRIVATE)
        val editor = sharedPreferences.edit()
        editor.putString("LOGIN_KEY", loginUser)
        editor.apply()
    }


    fun loadData(): String{
        val sharedPreferences = getSharedPreferences("TEST", Context.MODE_PRIVATE)
        var log = sharedPreferences.getString("LOGIN_KEY", "")

        return log.toString()
    }




}